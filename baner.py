#!/usr/bin/env python3

import sys
import re
import random
import itertools
from collections import Counter
from latex import *
from helpers import *



input_filename = "people.txt"
delimiter = "|"

seed_range = (0, sys.maxsize)

nr_lanes = 16 # We use this
ppl_pr_lane = 5


nr_attempts = 100000




def read_input():
    result = []
    with open(input_filename) as fin:
        for raw_line in fin:
            line = raw_line.strip()
            split = [s.strip() for s in line.split(delimiter)]

            name = split[0]
            study = Study[split[1]] if split[1] else Study.Unspecified
            start_year = int(split[2]) if split[2] else Year.Unspecified
            gender = Gender[split[3]] if split[3] else Gender.U

            p = Person(name, study, start_year, gender=gender)
            result.append(p)
    return result


def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in range(0, len(l), n):
        yield l[i:i+n]

def partition(lst, n):
    division = len(lst) / float(n)
    return [lst[int(round(division * i)): int(round(division * (i + 1)))] for i in range(n)]

def distribute(l):
    while True:
        random.shuffle(l)
        result = list(partition(l, nr_lanes))
        break # Possibly repeat experiment
    return result


def lane_stats(lane):
    stats = {}

    def add(*xs, key="study"):
        count = 0
        for x in xs:
            count += stats[key][x]
        return count


    stats["male"]   = 0
    stats["female"] = 0

    stats["study"] = {}
    stats["study"]["mat"] = 0
    stats["study"]["anv"] = 0
    stats["study"]["dat"] = 0
    stats["study"]["matok"] = 0
    stats["study"]["other"] = 0
    stats["study"]["unspecified"] = 0

    stats["start_year"] = {}
    stats["start_year"]["unspecified"] = 0

    for p in lane:
        if p.gender is Gender.M:
            stats["male"] += 1
        elif p.gender is Gender.K:
            stats["female"] += 1

        if p.study is Study.Mat:
            stats["study"]["mat"] += 1
        elif p.study is Study.Anv:
            stats["study"]["anv"] += 1
        elif p.study is Study.Dat:
            stats["study"]["dat"] += 1
        elif p.study is Study.MatOk:
            stats["study"]["matok"] += 1
        elif p.study is Study.Bio or p.study is Study.Kemi or p.study is Study.Fysik:
            stats["study"]["other"] += 1
        else:
            stats["study"]["unspecified"] += 1

        if p.start_year is Year.Unspecified:
            stats["start_year"]["unspecified"] += 1
        else:
            if not str(p.start_year) in stats["start_year"]:
                stats["start_year"][str(p.start_year)] = 0
            stats["start_year"][str(p.start_year)] += 1

    stats["study"]["basically_dat"] = add("dat", "anv", "other", "unspecified")
    stats["study"]["basically_mat"] = add("mat", "anv", "matok", "other", "unspecified")

    return stats




def check_lanes(lanes):
    for lane in lanes:
        stats = lane_stats(lane)

        # Don't allow a single male or female on a team
        if stats["male"] == 1 or stats["female"] == 1:
            return False

        # Don't allow a single dat or mat on a team
        if stats["study"]["basically_dat"] == 1 or stats["study"]["basically_mat"] == 1:
            return False

        c = Counter()

        for p in lane:
            if p.study is Study.Unspecified or p.start_year is Year.Unspecified:
                continue
            c[p.study, p.start_year] += 1

        for num_in_group in c.values():
            if num_in_group >= 3:
                return False

    return True


if __name__ == "__main__":
    if len(sys.argv) > 1:
        try:
            seed = int(sys.argv[1])
        except:
            print("Invalid seed from command-line")
            sys.exit(1)
    else:
        seed = random.randint(*seed_range)

    print("Seed:", seed)
    random.seed(seed)


    l = read_input()

    # Interesting statistics start
    stats = lane_stats(l)

    print("Male:   {}".format(stats["male"]))
    print("Female: {}".format(stats["female"]))
    print("Study:")
    for k, v in stats["study"].items():
        print("  {}: {}".format(k,v))
    print("Start year:")
    for k in sorted(stats["start_year"]):
        v = stats["start_year"][k]
        print("  {}: {}".format(k,v))
    # Interesting statistics end

    if len(l) > nr_lanes * ppl_pr_lane:
        print("The lanes will be overpopulated.")
        exit()


    success = False
    for i in range(nr_attempts):
        #print("Attempt {} of {}".format(i+1, nr_attempts))
        lanes = distribute(l)
        if check_lanes(lanes):
            success = True
            break


    if success:
        print("Found solution after {} attempts".format(i))
    else:
        print("Tried {} times to satisfy constraints, but failed every time.".format(nr_attempts))
        exit()

    print()

    doc = LatexDocument("document")

    doc.line(r"\begin{center}")
    doc.line(r"\textbf{\LARGE IMADA Fagråds Generalforsamling}")
    doc.line(r"\vskip0.5em")
    doc.line(r"\textbf{\LARGE Bowlingbanefordeling}")
    doc.line(r"\end{center}")
    doc.line(r"\vskip1em")

    for i, lane in enumerate(lanes):
        doc.line(r"\subsection*{Bane %d}" % (i+1))
        doc.line(r"\begin{multicols}{2}")
        for person in sorted(lane):
            doc.line((r"%s" % person.name))
            doc.line()
        doc.line(r"\end{multicols}")

    doc.skip()
    doc.line(r"{\color{white} Seed: %d}" % (seed))


    doc.compile() # Generate and compile a tex file in a folder
