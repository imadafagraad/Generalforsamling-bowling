from enum import Enum


class Study(Enum):
    Error = 0 # This should not happen

    Unspecified = 1 # The user explicitly did not supply this
    Undetermined = 2 # We could not with our rules determine this

    Mat = 3
    Anv = 4
    Dat = 5
    MatOk = 6
    Bio = 7
    Kemi = 8
    Fysik = 9


class Year(Enum):
    Unspecified  = -1
    Undetermined = -2


class Gender(Enum):
    U = 0
    M = 1
    K = 2


class Person:
    def __init__(self, name, study, start_year, email=None, gender=None):
        self.name = name
        self.study = study
        self.start_year = start_year

        self.email = email
        self.gender = gender

    def study_pretty(self, unspecified_placeholder="?"):
        if isinstance(self.study, Study):
            if self.study is Study.Unspecified:
                return unspecified_placeholder
            else:
                return self.study.name
        return self.study

    def year_pretty(self, unspecified_placeholder="?"):
        if isinstance(self.start_year, Year):
            if self.start_year is Year.Unspecified:
                return unspecified_placeholder
            else:
                return self.start_year.name
        return self.start_year

    def gender_pretty(self, unspecified_placeholder="?"):
        if self.gender:
            return self.gender.name
        else:
            return unspecified_placeholder

    def __str__(self):
        return "Person({}, {}, {})".format(self.name, self.study_pretty(), self.year_pretty())

    def __repr__(self):
        return str(self)

    def __lt__(self, other):
        return self.name < other.name
